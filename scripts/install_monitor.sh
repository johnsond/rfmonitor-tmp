#!/bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

libsdir2="/usr/local/lib/python2.7/dist-packages"
libsdir3="/usr/local/lib/python3.6/dist-packages"
maindir="/usr/local/sbin/"
configdir="/usr/local/etc/monitor"

# copy the python packages to 
cp -r ../src/monitor $libsdir2
cp -r ../src/monitor $libsdir3

# copy the main executable
cp ../src/monitor_main.py $maindir

# And the main app.
cp ../src/monitor_app.py $libsdir3

# copy the default config files
mkdir -p -- "$configdir"
cp ../src/monitor/calibration/cal_config.json "$configdir"
cp ../src/monitor/calibration/cal_data_ref.pkl "$configdir"
if [ ! -e $configdir/device_config.json ]; then
    cp ../src/monitor/device_config.json "$configdir"
fi

