#!/usr/bin/env python3.6

import monitor.calibration.tx_calibration_server as cal_tx_server
import time
import sys


if len(sys.argv) < 2:
    raise Exception("Please provide the transmitter number (0 - N-1 transmitters)")

tx_id  = int(sys.argv[1])

txCal = cal_tx_server.TX_Calibration_Server("/usr/local/etc/monitor/cal_config.json", tx_id)

