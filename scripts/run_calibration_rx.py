#!/usr/bin/env python3.6

import monitor.calibration.rx_calibration_client as cal_client
import time

rxCal = cal_client.RX_Calibration_Client("/usr/local/etc/monitor/cal_config.json")

ts = time.time()
rxCal.run_calibration()
tend = time.time()
print("elapsed time: {}".format(tend-ts))
