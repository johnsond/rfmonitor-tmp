#!/usr/bin/env python3

import monitor.util.device_output_viewer as device_viewer
import argparse,os,sys

def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--freq", default=1500e6, type=float)
    parser.add_argument("-r", "--rate", default=1e6, type=float)
    parser.add_argument("-a", "--avg", default=1000, type=int)
    parser.add_argument("-m", "--method", default="welch", type=str)
    parser.add_argument("-g", "--gain", default=70, type=int)
    parser.add_argument("-t", "--time", default="freq", type=str)
    parser.add_argument("-s", "--sweep", default=False, type=bool)
    

    return parser.parse_args()


"""
Entry point to the application
"""        
if __name__ == "__main__":
    args = parse_args()
    try:
        devview = device_viewer.DeviceViewer(avg=args.avg, gain=args.gain)
        #import pdb
        #pdb.set_trace()
        devview.start_streaming(args.freq,method=args.method, time_or_freq=args.time, sweep=args.sweep)

    except KeyboardInterrupt:
        print("\nInterrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
