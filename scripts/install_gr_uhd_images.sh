#!source

images_fname="uhd-images_003.010.003.000-release.tar.gz"
images_folder="uhd-images_003.010.003.000-release/share/uhd/images"
if [[ -f /tmp/$images_fname ]]; then
    echo "found image files"
else
    echo "did not find image files"
    exit 1
fi

UHD_IMAGES_DIR=/tmp/$images_folder
uhd_image_loader --args "type=b200" --fw-path $UHD_IMAGES_DIR/usrp_b200_fw.hex --fpga-path $UHD_IMAGES_DIR/usrp_b210_fpga.bin
