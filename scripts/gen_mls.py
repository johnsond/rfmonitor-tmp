#!/usr/bin/env python3

import monitor.calibration.pncode as code
import pickle
import matplotlib.pyplot as plt
from numpy.fft import fft, ifft, fftshift, fftfreq
import numpy as np
import scipy.io as io


mls = code.pn_code_gen(512)

with open('mls.pkl', 'wb') as f:
    pickle.dump(mls, f)

io.savemat('mls.mat', {'mls_seq':mls})

           
spec = fft(mls)
N = len(mls)
plt.plot(fftshift(fftfreq(N)), fftshift(np.abs(spec)), '.-')
plt.margins(0.1, 0.1)
plt.grid(True)
plt.show()
