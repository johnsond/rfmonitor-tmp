#!/bin/bash

cd ../test
python3 -m unittest test_device
python3 -m unittest test_receiver
python3 -m unittest test_data_server_out
python3 -m unittest test_data_message