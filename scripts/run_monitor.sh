#!/bin/bash

config="/usr/local/etc/monitor/device_config.json"
cal="/usr/local/etc/monitor/cal_data_ref.pkl"
mon_script='rfmonitor'
view_script='./view_monitor.py'
port='9000'
host='127.0.0.1' # local (for viewer)

echo "Starting viewer in background"
$view_script -s $host -p $port &

echo "Starting monitor"
$mon_script -s $host -p $port -c $config -i $cal

# exit cleanly by shutting down the background monitor when this script quits
trap 'kill $(jobs -p)' SIGINT EXIT