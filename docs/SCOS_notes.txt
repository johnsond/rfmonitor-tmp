Key components
1. Fully implements the SigMF signal capture description standard
2. Adds a new SCOS standard namespace
3. Provides a RESTful API from the sensor
4. Admin and User controls
5. Priority-based scheduling
6. Hardware agnostic
7. Security built into the implementation

---------------------------------------------------------------------------------------------
Benefits
- Provides a standard for describing a signal dataset
- Provides a standard and hardware-agnostic implementation for controlling an RF sensor device
- Admin and user priviliges may allow POWDER users to add their own sensor actions

--------------------------------------------------------------------------------------------
Notes:

Terminology
Action - A function that the sensor own implements and exposes to the API.
Acquisition - The combination of data and metadata created by an action
Schedule Entry - a name and an associated action
Task - an action that should be run at a specific time
User - an unprivileged account type that can create schedule entries and modify things they own.

----------------
Hardware Model (SensorDefinition object)
The hardware model is simple and is composed of the following four elements:
- Antenna - converts EM energy to a voltage
- Preselector - provides local calibration signals, RF filtering, and low-noise amplification
- Receiver - provides tuning, downconversion, sampling and digital signal processing 
- Host Controller - high level control over the other components


Antenna Model
- model - e.g. ARA CSB-16
- type - e.g. dipole
- low_frequency
- high_frequency
- gain (dBi)
- horizontal_gain_pattern (dBi)
- vertical_gain_pattern (dBi)
- horizontal_beam_width (degree-s, 3 dB)
- vertical_beam_width (degree-s, 3 dB)
- cross_polar_discrimination
- voltage_standing_wave_ratio (VSWR, volts)
- steerable (T/F)
- mobile (T/F)


Receiver Model
-model  (e.g. Ettus B210)
-low-frequency
-high-frequency
-noise_figure (dB)
-max_power (dB, max input power)


Preselector Model
-rf_paths - (array)


RFPath Model
-rf_path_number
-low_frequency_passband
-high_frequency_passband
-low_frequency_stopband
-high_frequency_stopband
-lna_gain
-lna_noise_figure
-cal_source_type (e.g. "calibrated noise source")


DigitalFilter Model
-type (e.g. "FIR")
-length (number of taps)
-frequency_cutoff (frequency at which the mag repsonse decreases from max by attenuation_cutoff)
-attenuation_cutoff (magnitude response threshold, dB)
-ripple_passband (amount of ripple in passband,  dB)
-attenuation_stopband (dB)
-frequency_stopband (Point in filter frequency response where stopband starts)

ScheduleEntry Model
-name (unique (to the sensor) name for schedule entry)
-start (requested start time in ISO-860 datetime format; if unspecified, will start ASAP)
-relative_stop (relative seconds after start when task ends)
-absolute_stop (absolute time (IS0-860) when task should end)
-interval (interval between tasks; if unspecified, the task will only run once)
-priority (lower numbers are higher priority, 10 is default)
-action (name of the action to be performed, e.g. "spectrum capture")

