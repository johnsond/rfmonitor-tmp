import socket
import sys
from . import net_constants as nc
from io import StringIO
import csv


class DataInServer():
    """DataOutServer 
    Represents the server that receives output from the monitor. 
    """

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.serversock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serversock.bind((self.ip, self.port))
        self.serversock.listen(5)
        
        # save parsed, received data to these lists
        self.devs = []
        self.ts = []
        self.freqs = []
        self.powers = []

    def listen(self):
        while(True):
            # receive data from the monitor client
            client_sock = self.serversock.accept()[0]
            recstr = data_recv(client_sock)
            client_sock.close()

            # parse received string
            f = StringIO(recstr)
            reader = csv.reader(f, delimiter=',')

            self.devs = []
            self.ts = []
            self.freqs = []
            self.powers = []
            for i, row in enumerate(reader):
                if i == 0:
                    continue  # skip the first row--just a header
                self.devs.append(row[0])
                self.ts.append(row[1])
                self.freqs.append(float(row[2]))
                self.powers.append(float(row[3]))


def data_recv(sock):
    chunks = ""
    while True:
        chunk = sock.recv(2048)
        if nc.TERM_MESSAGE in chunk.decode():
            term_ind = chunk.decode().find(nc.TERM_MESSAGE)
            chunks += chunk.decode()[:max(term_ind-1, 0)]
            return chunks
        chunks += chunk.decode()
