""" Defines constants used by the monitor/net package
"""
# how long to wait for a connection
CONNECT_TIMEOUT = 5

# used to initiate a message (header info)
INITIAL_MESSAGE = "portid,timestamp,frequency,power,center_freq\n"
    
# used to terminate a message
TERM_MESSAGE = "0,0,0,0" # terminate message'
