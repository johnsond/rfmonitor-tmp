import numpy as np

"""
Class that defines the structure of a message that will be sent to the server
"""

class DataMessage():
    def __init__(self, size=10):
        self.devs = np.array(["" for i in range(size)], dtype=object) # `object` so that strings can be arbitrary length
        self.timestamps = np.zeros(size, dtype=int)
        self.freqs = np.zeros(size, dtype=np.float64)
        self.powers = np.zeros(size, dtype=np.float64)

        self.count = 0

    def add_tuple(self, dev, ts, freq, power):
        """ Apply the appropriate processing here

        Parameters:
            dev - (string) device id 
            ts - (int) timestamp
            freq - (float) frequency 
            power - (float) power in decibels

        Returns:
            None

        """
        dev = str(dev)
        ts = int(ts)
        freq = np.float64(freq)
        power = np.float64(power)

        if self.count >= len(self.devs):
            self.devs = np.append(self.devs, dev) 
            self.timestamps = np.append(self.timestamps, ts)
            self.freqs = np.append(self.freqs, freq)
            self.powers = np.append(self.powers, power)
        else:
            self.devs[self.count] = dev
            self.timestamps[self.count] = ts
            self.freqs[self.count] = freq
            self.powers[self.count] = power

        self.count += 1

    def get_tuple_i(self, i):
        """ 

        Parameters:
            i - tuple number

        Returns:
            Tuple at i
        """
        if i >= self.count:
            raise Exception("[DATAMESSAGE] no tuple at index {}".format(i))
        return (self.devs[i], self.timestamps[i], self.freqs[i], self.powers[i])

    def get_count(self):
        return self.count

