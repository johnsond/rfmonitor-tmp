#!/usr/bin/python2
#
# Author: BCT
# Date last modified: 8/13/2018
#
# Use this to open and view a complex-valued binary (I/Q interleaved) data file

import sys
import datetime
import matplotlib.pyplot as plt
import numpy as np
import signal_debug
sys.path.insert(0, "../")
from radio import device, signal_utils as su

if len(sys.argv) < 2:
    print("Argument Error--")
    raise Exception ("Useage: rx_save_and_plot <freq> [1-plot, 0-don't plot] [1-save, 0-don't save]")

freq = float(sys.argv[1])

should_plot = False
should_save = False
if len(sys.argv) >= 3:
    should_plot = (sys.argv[2] == "1")
if len(sys.argv) >= 4:
    should_save = (sys.argv[3] == "1")
print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=")
print("-=-=-= Running collect on 2 channels")
print("-=-=-= Collecting at {:.2f} MHz".format(freq/1e6))
print("-=-=-= Plotting? {}".format('yes' if should_plot else 'no'))
print("-=-=-= Saving? {}".format('yes' if should_save else 'no'))
print("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=")

radio = device.Device(gain=70, num_samps_req=2**15)
bcast_sig = radio.get_samples(int(freq))[0]
fname1 = "rx1_collect_f{:.0f}M_{:%b%d-%H%M%S}".format(freq/1e6, datetime.datetime.now())
fname2 = "rx2_collect_f{:.0f}M_{:%b%d-%H%M%S}".format(freq/1e6, datetime.datetime.now())
sd = signal_debug.SignalDebug("./")
print("Shape of collected signal {}".format(bcast_sig.shape))
sd.add_vector(bcast_sig[0,:], fname1, plot=should_plot, save=should_save)
sd.add_vector(bcast_sig[1,:],fname2, plot=should_plot, save=should_save)
sd.save_vectors()
sd.plot_vectors()

#if should_plot: rx1r = np.real(bcast_sig[0,:]) rx1i =
#    np.imag(bcast_sig[0,:]) rx2r = np.real(bcast_sig[1,:]) rx2i=
#    np.imag(bcast_sig[1,:])
#
#    fig, axs = plt.subplots(1,2, sharex=True, sharey=True) x =
#    range(len(rx1r)) axs[0].plot(x, rx1r, x, rx1i)
#    axs[0].set_title('chan0 - RX1') axs[1].plot(x, rx2r, x, rx2i)
#    axs[1].set_title('chan1 - RX2') fig.suptitle('For Y-TX-chan1 on,
#    RX1>RX2', fontsize=16)
#
#    print("rx freq MHz: " + str(radio.usrp.get_rx_freq()/1e6))
#    print("rx gain: " + str(radio.usrp.get_rx_gain())) print("showing
#    plot") plt.show()


    
