#!/usr/bin/python2
#
# Author: BCT
# Date last modified: 8/13/2018
#
# Use this to open and view a complex-valued binary (I/Q interleaved) data file

import sys
import matplotlib.pyplot as plt
import numpy as np
import scipy

if len(sys.argv) < 3:
    print("Argument Error--")
    raise Exception ("Useage: plot_sample <file> <num_samples> [skip] [sample rate in Hz]")

skip = 0
if len(sys.argv) == 4:
    skip = int(sys.argv[3])

# including the sample rate will scale axes
sample_rate = 0
use_sample_rate = False
if len(sys.argv) == 5:
    sample_rate = int(sys.argv[4])
    use_sample_rate = True
    
filevec = sys.argv[1]
num_samples_to_plot = int(sys.argv[2])

if filevec.lower().endswith('.npdat'):
    data_file = np.load(filevec)
else:
    raise Exception("Only *.npdat type objects supported currently")

print(data_file.shape)
    
# We want an even number of samples
if num_samples_to_plot % 2 != 0:
    num_samples_to_plot  = num_samples_to_plot - 1 

c1 = data_file[0,:]
c2 = data_file[1,:]

realf = np.real(f)
imagf = np.imag(f)
mean = np.mean(f)
rms_real = np.sqrt(np.mean(np.square(realf)))
rms_imag = np.sqrt(np.mean(np.square(imagf)))


print("magnitude complex mean: " + str(np.abs(mean)))
print("real rms: " + str(rms_real))
print("imag rms: " + str(rms_imag))
print("num samples in file: " + str(len(f)))

# check that we don't exceed the file length with requested number of samples
x = range(num_samples_to_plot)
if skip + num_samples_to_plot > len(f):
    raise("Skip + num_samples requested exceeds number of samples in file")

if use_sample_rate:
    x = np.dot(np.array(x), 1.0/sample_rate)

# plot the time-domain signal
plt.subplot(211)
plt.plot(x, realf[skip:skip+num_samples_to_plot])
plt.plot(x, imagf[skip:skip+num_samples_to_plot])

# plot the frequency domain signal (shift so that 0 (DC) is in the middle of the axis)
win = np.hamming(num_samples_to_plot)
result = np.multiply(win, f[skip:skip+num_samples_to_plot])
F = np.fft.fftshift(np.fft.fft(result))
F = np.nan_to_num(10*np.log10(np.square(np.abs(F))))
half_intv_len = num_samples_to_plot / 2
X = range(-half_intv_len,half_intv_len)
if use_sample_rate:
    X = np.dot(np.dot(np.array(X),sample_rate),1.0/num_samples_to_plot)

plt.subplot(212)
plt.plot(X, F)
plt.show()
