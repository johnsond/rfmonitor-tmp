import numpy as np
import matplotlib.pyplot as plt
import pickle
import os

class SignalDebug():
    
    def __init__(self, save_dir):
        """ give this class numpy vectors and it will plot them or save them to file"""
        self.save_dir = save_dir
        self.vectors = {} # give each saved vector a name--used for plotting/saving

    def add_vector(self, vector, name, plot=True, save=True):
        """ Add a vector to the dictionary of vectors to be saved/plotted"""
        if type(vector) is not np.ndarray:
            vector = np.array(vector)
        self.vectors[name] = (vector, plot, save)
        

    def save_vectors(self):
        """ Save all the vectors that should be saved """
        for vkey in self.vectors:
            vtup = self.vectors[vkey]
            if vtup[2]:
                self._save_1_vector(vtup[0], vkey)

    def plot_vectors(self):
        """ Plot all the vectors that should be plotted 
    This function will block until the figures are closed."""
        for vkey in self.vectors:
            vtup = self.vectors[vkey]
            if vtup[1]:
                self._plot_1_vector(vtup[0], vkey)
        plt.show()

    def _plot_1_vector(self, vector, name):
        plt.figure()
        if vector.dtype is np.dtype('complex64') or vector.dtype is np.dtype('complex128'):
            plt.plot(range(len(vector)), np.real(vector), range(len(vector)), np.imag(vector))
        else:
            plt.plot(vector)
        plt.title(name)
        plt.show(block=False)
        
    def _save_1_vector(self, vector, name):
        fname = os.path.join(self.save_dir,  name + ".npdat")
        with open(fname, 'wb') as f:
            np.save(f, vector)


if __name__ == "__main__":
    save_dir = "sig_files/"
    sd = SignalDebug(save_dir)
    vec1 = np.sin(np.arange(0,1,0.1), dtype=np.complex64) + 1j*np.cos(np.arange(0,1,0.1))
    vec2 = np.sin(np.arange(0,1,0.1))

    sd.add_vector(vec1, 'complex_exp')
    sd.add_vector(vec2, 'sin')

    sd.save_vectors()
    sd.plot_vectors()
    #plt.show()
