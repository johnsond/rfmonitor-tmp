import matplotlib.pyplot as plt
import numpy as np

class MultiPlotViewer():
    def __init__(self, num_x, num_y, interval=100):
        self.fig, self.axes = plt.subplots(num_x, num_y, \
                                          sharex=True, sharey=True)
        self.timer = self.fig.canvas.new_timer(interval=interval)
        self.timer.add_callback(self.update_axes, self.axes)
    
    def set_data(self, data):
        self.data = data

    def get_data(self):
        return self.data

    def update_axes(self, axes):
        data = self.get_data()
        axidx = 0
        for axrow in axes:
            for axentry in axrow:
                axentry.cla() # clear old plot
                axentry.plot(data[axidx]) # set the new data
                axentry.figure.canvas.draw() # draw on these axes
                axidx += 1

    def start_plot(self):
        self.timer.start()
        plt.show()

if __name__ == "__main__":
    import time
    from multiprocessing import Process
    
    mpv = MultiPlotViewer(2,2)
    mpv.set_data([np.random.randn(50), \
                  np.random.randn(50), \
                  np.random.randn(50), \
                  np.random.randn(50)])

    def cont_set_data(mpv):
        idx = 0
        while(idx < 10):
            data = [np.random.randn(50), \
                    np.random.randn(50), \
                    np.random.randn(50), \
                    np.random.randn(50)]
            mpv.set_data(data)
            time.sleep(2)
            idx +=1
            print(idx)
            
    p = Process(target = cont_set_data, args=(mpv,))
    p.start()
    mpv.start_plot()
    
