from sigmf.sigmffile import SigMFFile
from sigmf.archive import SigMFArchive
from datetime import datetime


class Storage():
    def __init__(self):
        pass

    def load_weights(self):
        pass

    def backup_weights(self):
        pass
        
    def store_spectrum_sigmf(self, bins, labels, metadata):
        # reshape bins and labels
        bins_fl = bins.flatten()
        labels_fl = labels.flatten()

        # write the temporary data file
        temp_fname = 'bins.tmp'
        with open(temp_fname, 'wb') as bins_file:
            bins_file.write(bins_fl)
        
        # build a sigmf file
        # NOTE:
        # SigMF may not be the right tool right now. The intended way to support
        # continously varying annotations is to support multiple sigmf recordings
        # Development of this is not in the main branch and seems to have stalled.
        #
        # Instead, when this method is used, I'll just save them as two separate
        # sigmf recordings.
        description = "Spectral capture"
        sigmf_f = SigMFFile()
        sigmf_f.set_global_field("core:datatype", "cf32_le")
        sigmf_f.set_global_field("core:sample_rate", metadata['samp_rate'])
        sigmf_f.set_global_field("core:description", description)
        capture_md = {
            "core:frequency": 0,
            "core:time": datetime.isoformat(datetime.utcnow()) + 'Z'
        }
        sigmf_f.add_capture(start_index=0, metadata=capture_md)
        data = temp_fname
        sigmf_f.set_data_file(data)

        fname = "capture_" + str(datetime.now())
        
        SigMFArchive(sigmf_file, name=fname)
        
         # write the temporary data file
        temp_fname = 'freqs.tmp'
        with open(temp_fname, 'wb') as freqs_file:
            freqs_file.write(freqs_fl)

        description = "Frequency labels"
        sigmf_f = SigMFFile()
        sigmf_f.set_global_field("core:datatype", "cf32_le")
        sigmf_f.set_global_field("core:sample_rate", 0)
        sigmf_f.set_global_field("core:description", description)
        capture_md = {
            "core:frequency": 0,
            "core:time": datetime.isoformat(datetime.utcnow()) + 'Z'
        }
        sigmf_f.add_capture(start_index=0, metadata=capture_md)
        data = temp_fname
        sigmf_f.set_data_file(data)

        fname = "freq_labels_" + str(datetime.now())
        
        SigMFArchive(sigmf_file, name=fname)
        
