import numpy as np
import numpy.matlib as mb
import scipy.signal as ssig
import ntplib

import pickle, xmlrpc.client, time, datetime, json

from .pncode import pn_code_gen
from .calibration_measurements import Calibration_Measurements
#from .calibration_measurements import Calibration_Signals
from ..radio import device, signal_utils as su, constants as C
from ..util import signal_debug 
from ..logs import logs

DURATION=3
LOGS_DIR="/tmp/rx_cal_logs/"


"""
TODO
   * save [rx1,rx2]-x, [rx1,rx2]-y, known signal
   * add welch averaging
   * known signal is now extended, welch averaged (?) Test this
"""

class RX_Calibration_Client():

    def __init__(self, config_file):
        """Create a receiver calibration client. 
        The client can request transmissions from transmitter server(s) through RPCs.

        This method sets up the connection to the transmitter and checks to see if the RPC server is running (xmlrpclib has lazy checking of connection
        status). It then creates a radio Device and creates a copy of the expected signal. 

        Theory of Operation: 
        | The transmitter provides (white -> flat) spectrum excitation for about 30 MHz of spectrum at a time. 
        | The receiver compares the received signal on each channel against the known sent signal and attempts to find the response of the couplers and 
        | antennas 
        |
        | At the receiver, rx1=h1(x); rx2=h2(x), where h1, and h2 are the system impulse responses. Deconvolution is used to find these responses. 
        | In the frequency domain, H1=RX1/X; H2=RX2/X. 
        | 
        """
        import pdb
        pdb.set_trace()

        self.logs = logs.Logs(log_directory=LOGS_DIR)
        self.print_and_log("[CAL] created log file at {}".format(LOGS_DIR))
        

        # generate the known signal DFT(MLS)
        self.XY = gen_source_data(C.FFT_SIZE, C.CAL_K) 
        
        self.num_matrices=C.N_BINS_TOTAL # ~ 200 steps and 500 freq bins per step -> 100_000 2x2 matrices
        self.num_cf_steps = len(C.CENTER_FREQS)
        self.num_bins = C.N

        # synchronize time
        c = ntplib.NTPClient()
        try:
            response = c.request('us.pool.ntp.org', version=3)
        except:
            # try one more time:
            time.sleep(5)
            response = c.request('us.pool.ntp.org', version=3)

        self.t_offset = response.offset
        self.print_and_log("[CAL] time offset {}".format(self.t_offset))
        self.print_and_log("[CAL] system time  {}".format(datetime.datetime.now().time()))
        self.print_and_log("[CAL] adjusted time  {}".format(self.get_time_now()))
        self.print_and_log("[CAL] TX radio ready")

        # open config file 
        with open(config_file, 'r') as cf:
            config = json.load(cf)  
        
        self.tx_servers_config = config['transmitters']
        self.tx_servers_exper = self.tx_servers_config['experimental']
        self.tx_server_incd = self.tx_servers_config['incident']
        self.n_exp_tx = len(self.tx_servers_exper)
        self.this_receiver = config['receiver']

        connected_mon_antennas = self.get_monitor_antennas(self.this_receiver['antennas'])

        # set up local USRP, buffers, etc.
        self.radio = device.Device(RXA=connected_mon_antennas['RXA'],
                                RXB=connected_mon_antennas['RXB'])
        if not self.check_serial_match():
            self.print_and_log("[CAL] !WARNING! Serial number of monitor SDR does not match configuration file entry")
        self.sd = signal_debug.SignalDebug("debug/")
        self.print_and_log("USRP device created")

        self.print_and_log("[CAL] {} experimental transmitter(s) is(are) configured".format(self.n_exp_tx))
        self.print_and_log("[CAL] will compute {} matrices per transmitter".format(self.num_matrices))
        self.print_and_log("[CAL] saving data to {}".format(C.CAL_DATA_FNAME))
        
        self.tx_proxies = []
        self.tx_antennas = []
        for ii in range(self.n_exp_tx):
            self.tx_antennas.append(self.get_tx_antenna(self.tx_servers_exper[ii]['antennas']))
        self.tx_incd_antenna = self.get_tx_antenna(self.tx_server_incd['antennas'])


        # create a measurement object for each experimental transmitter
        self.measurements = [Calibration_Measurements(self.tx_servers_exper[ii]['host'], C.CENTER_FREQS, self.num_bins) for ii in range(self.n_exp_tx)]
        self.signal_record = [Calibration_Measurements(self.tx_servers_exper[ii]['host'], C.CENTER_FREQS, self.num_bins) for ii in range(self.n_exp_tx)]
        self.signal_record.XY = self.XY

        # create a connection to each experimental transmitter server
        for idx, tx in enumerate(self.tx_servers_exper):
            print("[CAL] setting up proxy for transmitter {}/{}".format(idx+1, self.n_exp_tx))
            try:
                self.print_and_log("[CAL] attempting to connect to {}:{}".format(tx['ip'], tx['port']))
                proxy = xmlrpc.client.ServerProxy("http://{}:{}/".format(tx['ip'], tx['port']))
                #proxy = xmlrpc.client.ServerProxy('http://155.98.47.67:9998') 
                self.print_and_log("[CAL] experimental transmitter proxy created")
                self.tx_proxies.append(proxy)
            except:
                raise Exception("[CAL] could not connect to server on the transmitter node. Is it running?")


        # create a connection to the incident transmitter receiver
        self.print_and_log("[CAL] setting up proxy for the incident transmitter")
        try:
            tx = self.tx_server_incd
            self.print_and_log("[CAL] attempting to connect to {}:{}".format(tx['ip'], tx['port']))
            self.tx_incd_proxy = xmlrpc.client.ServerProxy("http://{}:{}/".format(tx['ip'], tx['port']))
            #proxy = xmlrpc.client.ServerProxy('http://155.98.47.67:9998') 
            self.print_and_log("[CAL] incident transmitter proxy created")
            
        except:
            raise Exception("[CAL] could not connect to server on the transmitter node. Is it running?")

        # test the connection to each of the transmitters
        check = 0
        for tx in self.tx_proxies:
            check += tx.is_running()
        check += self.tx_incd_proxy.get_params()

        self.print_and_log("[CAL] {} TX servers are connected and responding".format(check))
        
    def run_calibration(self, save_mat=False):
        """ standard calibration starter
        """ 
        errors = 0
        for tx_idx, tx_proxy in enumerate(self.tx_proxies):
            for cf_idx,cf in enumerate(C.CENTER_FREQS):
                errort,_,_ = self._calibrate_freq_based(tx_proxy,tx_idx, cf, cf_idx)
                errors += errort
                self.print_and_log("[CAL] TX: {}/{} Freq: {:.2f} MHz, {:3.2f}% complete. {} errors \n".format(tx_idx+1, len(self.tx_proxies),
                                                                                      cf/1e6, 100*cf_idx/C.NUM_STEPS, errors))
            self.print_and_log("[CAL] Complete!")
            
            # save matrix measurements
            with open("matrix_{}_".format(tx_idx)+C.CAL_DATA_FNAME, 'wb') as calF:
                pickle.dump(self.measurements[tx_idx], calF)

            # save signal measurements
            with open("signals_record_{}.pkl".format(tx_idx), 'wb') as calsigF:
                pickle.dump(self.signal_record[tx_idx], calsigF)

        if save_mat:
            self.save_data_to_mat_files(str(self.this_receiver))            

        return errors

    def _calibrate_freq_based(self, tx_proxy, tx_idx, cf, cf_idx, average_method='welch'):
        """ For each frequency band, compute complex coefficient representing the relationship     
        between the transmitted signal TX, the received signal RX, the first monitor channel 
        R1, and the second monitor channel R2.
        
        """

        # there should be N measurement points per center frequency (1 per bin of the FFT)
        measure_idx_start = cf_idx * C.N
        measure_idx_end = cf_idx * C.N + C.N
        msr_idxs = np.arange(measure_idx_start, measure_idx_end, dtype=int) 
        self.print_and_log("[CAL] calibrating for {:.2f} MHz to {:.2f} MHz ({} steps)".format((msr_idxs[0]+cf)/1e6, (msr_idxs[-1]+cf)/1e6, len(msr_idxs)))
        errors = 0
        
        # We start by calibrating X (experimenter transmitting)
        # We then calibrate for Y (incident energy received)
        #psd_save = np.zeros((4,C.N), dtype=np.float32)
        for XorY in ["X", "Y"]:
            
            # start transmitting on TX1
            if XorY == "X":
                chan = self.tx_antennas[tx_idx]
                self.print_and_log("[CAL] Requesting experimenter broadcast at time {} on channel: {}".format(self.get_time_now(), chan))
                timestart = tx_proxy.broadcast_chan0(float(cf), DURATION) if chan == 0 else tx_proxy.broadcast_chan1(float(cf), DURATION)
            else:
                chan = self.tx_incd_antenna
                self.print_and_log("[CAL] Requesting incident broadcast at time {} on channel: {}".format(self.get_time_now(), chan))  
                timestart = self.tx_incd_proxy.broadcast_chan0(float(cf), DURATION) if chan == 0 else self.tx_incd_proxy.broadcast_chan1(float(cf), DURATION)

            # wait for broadcast to start
            time.sleep(2) 
            
            # gather measurements from both input ports
            self.print_and_log("[CAL] Sampling broadcast at time: {}".format(self.get_time_now()))
            #dft = self.radio.get_dft_avg(cf, C.CAL_N, C.CAL_K) # old method
            # n_time = N*avg
            psd, labels = self.radio.get_spectrum(cf, N=C.FFT_SIZE, est_method=average_method, avg=C.CAL_K)

            # find the response for each transmitter
            R1 = psd[0,:]
            R2 = psd[1,:]

            C1 = np.divide(R1, self.XY)
            C2 = np.divide(R2, self.XY)

            # store the center frequency information
            self.measurements[tx_idx].frequencies[cf_idx, :] = labels
            self.signal_record[tx_idx].frequencies[cf_idx, :] = labels

            if XorY == "X":
                # save a,c
                self.measurements[tx_idx].set_a(C1, cf_idx)
                self.measurements[tx_idx].set_c(C2, cf_idx)
                #self.print_and_log("[CAL] mean a:{:.4f},c:{:.4f}".format(np.mean(C1), np.mean(C2)))
                self.signal_record.Xs[tx_idx] = psd
            else:
                # save c,d
                self.measurements.set_b(C1, cf_idx)
                self.measurements.set_d(C2, cf_idx)
                #self.print_and_log("[CAL] mean b:{:.4f},d:{:.4f}".format(np.mean(C1), np.mean(C2)))
                self.signal_record.Ys[tx_idx] = psd
            
  
            Ebcast1 = su.energy(R1)
            Ebcast2 = su.energy(R2)
            errors += 1 if (Ebcast1 < 100.0 or Ebcast2 < 100.0) else 0
            
            self.print_and_log("[CAL] E_R1:{:.2f}, E_R2:{:.2f}".format(Ebcast1,Ebcast2))
            time.sleep(1)
           
        return errors
    
    def run_calibration_debug(self, freqs=C.CENTER_FREQS[50:52]):
        """ Runs only on two frequencies. Saves results to file. 
        # freqs: array([1.482400e+09, 1.510048e+09])
        """
        for tx_idx, tx_proxy in enumerate(self.tx_proxies):
            for cf_idx,cf in enumerate(freqs):
                self._calibrate_freq_based(tx_proxy,tx_idx, cf, cf_idx)

        
    def save_data_to_mat_files(self, filename_base):
        """ Save the files to .mat
        These can easily be opened in MATLAB
        """
        for ii in range(self.n_exp_tx):
            self.measurements[ii].save_data_to_mat_files(filename_base+"_"+str(ii)) 
            self.signal_record[ii].save_data_to_mat_files(filename_base+"_"+str(ii))       

    def _calibrate_time_based(self, transmitter_proxy, freq, index, cal_measurements):
        raise Exception("[CAL] CURRENTLY DEPRECATED. SUPPORT NOT LIKELY TO RETURN")

    def get_time_now(self):
        """Return the best estimate of the real time
        """
        timenow = datetime.datetime.now() + datetime.timedelta(seconds=self.t_offset)
        timenow_str = str(timenow.time()) # RPC can't marshall datetime objects
        return timenow_str

    def get_monitor_antennas(self, mon_antenna_config):
        # check the antenna configuration given
        connected_config = {}
        if mon_antenna_config['ant_txrxA'] != "None" and mon_antenna_config['ant_rx2A'] == "None":
            connected_config['RXA'] = "TX/RX"
        elif mon_antenna_config['ant_rx2A'] != "None" and mon_antenna_config['ant_txrxA'] == "None":
            connected_config['RXA'] = "RX2"
        else:
            raise Exception("[CAL] Monitor config needs exactly one antenna configured for channel A (0)")
        
        if mon_antenna_config['ant_txrxB'] != "None" and mon_antenna_config['ant_rx2B'] == "None":
            connected_config['RXB'] = "TX/RX"
        elif mon_antenna_config['ant_rx2A'] != "None" and mon_antenna_config['ant_txrxB'] == "None":
            connected_config['RXB'] = "RX2"
        else:
            raise Exception("[CAL] Monitor config needs exactly one antenna configured for channel B (1)")
            
        return connected_config

    def get_tx_antenna(self, tx_antenna_config):
        # check the antenna configuration given
        if (tx_antenna_config['ant_txrxA'] != "None" and tx_antenna_config['ant_rx2A'] == "None" and 
            tx_antenna_config['ant_txrxB'] == "None" and tx_antenna_config['ant_rx2B'] == "None"):
            return  0
        elif (tx_antenna_config['ant_rx2A'] == "None" and tx_antenna_config['ant_txrxA'] == "None" and 
              tx_antenna_config['ant_txrxB'] != "None" and tx_antenna_config['ant_rx2B'] == "None"):
            return  1
        else:
            raise Exception("[CAL] Transmitter config needs exactly one antenna configured")
            

    def check_serial_match(self):
        configserial = self.this_receiver['sdr_serial']
        reportserial = self.radio.get_serial_number()
        if configserial != reportserial:
            self.print_and_log("[CAL] config serial number: {}. Reported serial number: {}".format(configserial, reportserial))
            return False
        else:
            return True

    def print_and_log(self, message):
        """
        """
        self.logs.log_other(message)
        print(message)

        

def gen_source_data(seq_len, avg):
    """Create a spectrally flat signal 
    """
    signal_t = pn_code_gen(seq_len)
    if avg > 1:
        signal_t_ext = extend_wf(signal_t, avg)
        signal, _ = su.psd_welch(seq_len, signal_t_ext, 0, C.N_REMOVE, C.MAX_INST_BW)
    else:
        signal = su.psd(seq_len, signal_t, 0, C.N_REMOVE, C.MAX_INST_BW)
    return signal

def extend_wf(wf, periods):
    """make N periods of the waveform
    """
    signal = mb.repmat(wf, 1, periods)
    signal = np.squeeze(signal)
    return signal
