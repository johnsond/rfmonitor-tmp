""" Run this on a transmitting node (experimental nuc)
"""

import xmlrpc.server
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler
from .pncode import pn_code_gen
from ..radio import constants as C
from ..logs import logs

import numpy as np
import time, datetime
from time import ctime
import os
import json
import ntplib 

import uhd

"""
Threading is used instead of multiprocessing because threads in this class are not really affected by the the global interpreter lock limitation.
They are I/O bound and call C++ code, so other python code can concurrently run. In addition, we don't want to use a separate process because
The uhd runtime will detect a new PID and set up the link again. 
"""
import threading
from multiprocessing import Process


# Calibration server TX

# setup the transmitter
GAIN = 70
DURATION = 2  # sec
RATE=C.MAX_INST_BW
LOGS_DIR="/tmp/tx_cal_logs/"
WARNC = '\033[93m'
NORMC = '\033[0m'

# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/', '/RPC2',) 


class TX_Calibration_Radio():
    def __init__(self, tx_gain):
        self.duration = DURATION
        self.bcast_lock = threading.Lock()
        # start the transmitter
        self.usrp = uhd.usrp.MultiUSRP()
        self.last_tx_freq = 0
        self.wf = pn_code_gen(C.FFT_SIZE)
        self.tx_gain = tx_gain

        self.logs = logs.Logs(log_directory=LOGS_DIR)
        self.print_and_log("[CAL] created log file at {}".format(LOGS_DIR))

        # synchronize time
        c = ntplib.NTPClient()
        try:
            response = c.request('us.pool.ntp.org', version=3)
        except:
            # try one more time
            time.sleep(5)
            response = c.request('us.pool.ntp.org', version=3)

        self.t_offset = response.offset 
        self.print_and_log("[CAL] time offset {}".format(self.t_offset))
        self.print_and_log("[CAL] system time  {}".format(datetime.datetime.now().time()))
        self.print_and_log("[CAL] adjusted time  {}".format(self.get_time_now()))
        self.print_and_log("[CAL] TX radio ready")

    def get_waveform(self):
        # can't marshal complex type so send a 2d array as a list
        arr = np.zeros((2, len(self.wf)))
        arr[0,:] = np.real(self.wf)
        arr[1,:] = np.imag(self.wf)
        return arr.tolist()

    def is_running(self):
        """ Simple positive response from server
        """
        return 1
    
    def get_params(self):
        """ Basic parameters about the transmitter
        """
        return {'duration': self.duration, 'rate':RATE}

    def get_actual_freq(self):
        # ints in GHz range are too big to send over RPC
        return float(self.last_tx_freq)

    def get_time_now(self):
        """Return the best estimate of the real time
        """
        timenow = datetime.datetime.now() + datetime.timedelta(seconds=self.t_offset)
        timenow_str = str(timenow.time()) # RPC can't marshall datetime objects
        return timenow_str

    def broadcast(self,frequency, duration, channel, gain):
        """ This is the thread that runs broadcast """
        # begin broadcasting
        ts = datetime.datetime.now()
        print("[CAL] [BROADCAST] Starting at {}".format(self.get_time_now()))
        self.usrp.send_waveform(self.wf, duration, frequency, RATE, (channel,), gain)

        # report actual parameter values
        act_freq = self.usrp.get_tx_freq()
        act_gain = self.usrp.get_tx_gain()
        act_rate = self.usrp.get_tx_rate()
        self.print_and_log("[CAL] [BROADCAST] Transmitted at frequency: {:.2f}, reported frequency: {:.2f}, for {} seconds".format(frequency/1e6, act_freq/1e6, duration))
        self.print_and_log("[CAL] [BROADCAST] req gain: {}, reported gain: {}, req rate: {:.2f} MSps. reported rate: {} MSps".format(gain, act_gain, RATE/1e6, act_rate/1e6))
        tend = datetime.datetime.now()
        self.print_and_log("[CAL] [BROADCAST] Stopped at {}".format(self.get_time_now()))
        self.print_and_log("[CAL] [BROADCAST] Actual duration: {} \n".format(tend-ts))
        self.bcast_lock.release()
    
    def broadcast_chan0(self, frequency, duration):
        """ Broadcasts the given waveform on only channel 1
        """
        self.print_and_log("[CAL] [BROADCAST-C0] sending on channel 0 at freq: {:.2f} MHz".format(frequency/1e6))

        freq_req = uhd.libpyuhd.types.tune_request(frequency)
        self.usrp.set_tx_freq(freq_req)
        self.last_tx_freq = self.usrp.get_tx_freq()
        self.bcast_lock.acquire_lock()
        timestart = self.get_time_now()
        tbcast = threading.Thread(target=self.broadcast, name="broadcast-0",args=(frequency, duration, 0, self.tx_gain))
        #tbcast = Process(target=self.broadcast, name="broadcast-0",args=(frequency, duration, 0)) 
        tbcast.start()
        return str(timestart)
        
    def broadcast_chan1(self, frequency, duration):
        """ Broadcasts the given waveform on only channel 1
        """
        self.print_and_log("[CAL] [BROADCAST-C1] sending on channel 1 at freq: {:.2f} MHz".format(frequency/1e6))
        freq_req = uhd.libpyuhd.types.tune_request(frequency)
        self.usrp.set_tx_freq(freq_req)
        self.last_tx_freq = self.usrp.get_tx_freq()
        self.bcast_lock.acquire_lock()
        timestart = self.get_time_now()
        tbcast = threading.Thread(target=self.broadcast, name="broadcast-1",args=(frequency, duration, 1, self.tx_gain))
        #tbcast = Process(target=self.broadcast, name="broadcast-0",args=(frequency, duration, 1)) 
        tbcast.start()
        return str(timestart)

    def print_and_log(self, message):
        """
        """
        self.logs.log_other(message)
        print(message)
    
    def check_serial_match(self, config):
        configserial = config['sdr_serial']
        info = self.usrp.get_usrp_rx_info()
        reportserial =  info['mboard_serial']
        if configserial != reportserial:
            self.print_and_log("[CAL] config serial number: {}. Reported serial number: {}".format(configserial, reportserial))
            return False
        else:
            return True



def TX_Calibration_Server(config_file, id):

    # parse JSON config file 
    with open(config_file, 'r') as cf:
        config = json.load(cf)  
    tx_servers_config = config['transmitters']
    tx_servers_exper = tx_servers_config['experimental']
    if id >= len(tx_servers_exper):
        raise Exception("[CAL] invalid ID number given {}. {} transmitter servers configured".format(id, len(tx_servers_exper)))

    # Use the incident if -1 is given as the ID
    print("[CAL] ID: " + str(id))
    if id < 0:
        this_tx = tx_servers_config['incident']
        print("[CAL] Configuring as incident transmiter ")
    else:
        this_tx = tx_servers_exper[id]
        print("[CAL] Configuring as experimental transmiter ")

    
    # set up RPC server
    print("[CAL] attempting to bind to  {}:{} ... ".format(this_tx['ip'], this_tx['port']))
    ip = "0.0.0.0"
    port = int(this_tx['port'])
    try:
        gain = int(this_tx['gain'])
    except KeyError:
        # use default
        gain = GAIN    

    # create the radio instance that controls broadcasts
    tx_server_radio = TX_Calibration_Radio(tx_gain=gain)
    
    if not tx_server_radio.check_serial_match(this_tx):
        tx_server_radio.print_and_log(WARNC + "[CAL] !WARNING! Serial number of monitor SDR does not match configuration file entry" + NORMC)


    # Create server and serve methods from radio instance
    with SimpleXMLRPCServer((ip, port),
                        requestHandler=RequestHandler) as server:
        server.register_introspection_functions()
        
        print("[CAL] listening on  {}:{} ... ".format(ip, port))

        server.register_function(tx_server_radio.get_actual_freq, "get_actual_freq")
        server.register_function(tx_server_radio.get_params, "get_params")
        server.register_function(tx_server_radio.broadcast_chan0, "broadcast_chan0")
        server.register_function(tx_server_radio.broadcast_chan1, "broadcast_chan1")
        server.register_function(tx_server_radio.get_waveform, "get_waveform")
        server.register_function(tx_server_radio.get_time_now, "get_time_now")
        print("[CAL] server ready")

        # Run the server's main loop
        server.serve_forever()





 

