# Calibration

TX Calibration Server - operates on an experimental nuc and provides a known signal output.

RX Calibration Client - application requests transmissions from the cal server at a specified frequency.