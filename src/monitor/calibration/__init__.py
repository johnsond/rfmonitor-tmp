
import time
import sys
import argparse

def parse_args():
    """Parse the command line arguments"""
    def_cal_file = "cal_config.json"
    try:
        from monitor.app import get_default_config_path
        def_cal_file = get_default_config_path("cal_config.json")
    except Exception:
        pass
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server", default=False, action='store_true')
    parser.add_argument("-i", "--tx-number", default=-1, type=int)
    parser.add_argument("-c", "--client", default=False, action='store_true')
    parser.add_argument("-f", "--calfile", default=def_cal_file, type=str)
    parser.add_argument("-d", "--debug", default=False, action='store_true')
    return parser.parse_args()

"""
Entry point to the calibration application.
"""
def main():
    args = parse_args()
    if (args.server and args.client) or not (args.server or args.client):
        print("Error: must set either server (-s) or client (-c) mode",
              file=sys.stderr)
        exit(-1)
    if args.server and not (isinstance(args.tx_number,int)
                            and args.tx_number >= 0):
        print("Error: in server mode, must provide tx-number (transmitter number, 0-(n-1) transmitters)",
              file=sys.stderr)
        exit(-1)
    client = None
    if args.server:
        import monitor.calibration.tx_calibration_server as tx_server
        server = tx_server.TX_Calibration_Server(args.calfile,args.tx_number)
    else:
        import monitor.calibration.rx_calibration_client as rx_client
        client = rx_client.RX_Calibration_Client(args.calfile)
        ts = time.time()
        if args.debug:
            client.run_calibration_debug()
        else:
            client.run_calibration()
        tend = time.time()
        print("Elapsed time: {}".format(tend-ts))
    exit(0)
