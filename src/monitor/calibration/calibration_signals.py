import numpy as np

class Calibration_Signals():
    """
    This is a set of arrays of 2 x 2 matrices (stored as a list) with helper methods for setting and getting individual parameters.
   
    DIM0 - The device (e.g. self.iso_matrices[0, : :] returns the matrices for dev0)
    DIM1 - The center_frequency (e.g. self.iso_matrices[0, 0 :] returns the matrix for dev0 collected at the first center frequency)
    DIM2 - The matrix elements (e.g. self.iso_matrices[0, 0, 0] returns the first matrix element (a) for dev0 at the first center frequency)
    
    It also stores the center frequency associated with a bin
    C = [[ a b ]  [ a b ]  _ _ _ [ a b ]]
        [[ c d ], [ c d ],       [ c d ]]

    f = [  f1,      f2,    . . .  fn   ]


    Example: 
    num_bins => 460
    num_center_frequencies => 214
    """
    def __init__(self, device_id, center_frequencies, num_bins):
        # ID of each device 
        self.device_id = device_id

        # each segment indexed by center frequency will have N bins
        self.num_bins = num_bins

        # save the center frequencies
        self.center_frequencies = center_frequencies

        # [centerfreqs, labels]
        freq_label_dims = (len(center_frequencies), num_bins)
        self.frequencies =  np.zeros(freq_label_dims, dtype=np.float32)           

        # record of the signals recorded
        self.Xs = np.zeros(freq_label_dims, dtype=np.float32)  
        self.Ys = np.zeros(freq_label_dims, dtype=np.float32)  
        self.XY = np.zeros((num_bins, ), np.float32)                             
    
    
    def save_as_mat(self,fname_base):
        import scipy.io as io
        fb = str(fname_base)
        io.savemat(fb+"_signals.mat", {'XY':self.XY,
            'Xs':self.Xs,
            'Ys':self.Ys,
            'freqs':self.frequencies})

