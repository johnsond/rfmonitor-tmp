import numpy as np

class Calibration_Measurements():
    """
    This is a set of arrays of 2 x 2 matrices (stored as a list) with helper methods for setting and getting individual parameters.
   
    DIM0 - The device (e.g. self.iso_matrices[0, : :] returns the matrices for dev0)
    DIM1 - The center_frequency (e.g. self.iso_matrices[0, 0 :] returns the matrix for dev0 collected at the first center frequency)
    DIM2 - The matrix elements (e.g. self.iso_matrices[0, 0, 0] returns the first matrix element (a) for dev0 at the first center frequency)
    
    It also stores the center frequency associated with a bin
    C = [[ a b ]  [ a b ]  _ _ _ [ a b ]]
        [[ c d ], [ c d ],       [ c d ]]

    f = [  f1,      f2,    . . .  fn   ]


    Example: 
    num_bins => 460
    num_center_frequencies => 214
    """
    def __init__(self, device_id, center_frequencies, num_bins):
        # ID of each device 
        self.device_id = device_id

        # each segment indexed by center frequency will have N bins
        self.num_bins = num_bins

        # save the center frequencies
        self.center_frequencies = center_frequencies

        # [centerfreqs, num_bins, matrix_elems]
        iso_matrix_dims = (len(center_frequencies), num_bins, 4)
        self.iso_matrices = np.zeros(iso_matrix_dims, dtype=np.float32)

        # [centerfreqs, labels]
        freq_label_dims = (len(center_frequencies), num_bins)
        self.frequencies =  np.zeros(freq_label_dims, dtype=np.float32)                                        
    
    def set_a(self, a_list, cf_idx):
        """ For each center frequency set the a values
        """
        self.iso_matrices[cf_idx,:, 0]=a_list

    def set_b(self, b_list, cf_idx):
        """ For each center frequency set the b values
        """
        self.iso_matrices[cf_idx,:, 1]=b_list

    def set_c(self, c_list, cf_idx):
        """ For each center frequency set the c values
        """
        self.iso_matrices[cf_idx,:, 2]=c_list

    def set_d(self, d_list, cf_idx):
        """ For each center frequency set the d values
        """
        self.iso_matrices[cf_idx,:, 3]=d_list


    def get_matrix(self, cf_idx):
        t = np.zeros((self.num_bins, 2,2), dtype=np.float32)
        t[:,0,0] = self.iso_matrices[cf_idx,:,0]
        t[:,0,1] = self.iso_matrices[cf_idx,:,1]
        t[:,1,0] = self.iso_matrices[cf_idx,:,2]
        t[:,1,1] = self.iso_matrices[cf_idx,:,3]
        labels = self.frequencies[cf_idx]
        return t, labels

    def get_matrices(self, center_frequency_indices):
        m = len(center_frequency_indices)
        t = np.zeros((m,self.num_bins,2,2), dtype=np.float32)
        labels = np.zeros((m,self.num_bins,2,2), dtype=np.float32)
        for ii in range(len(center_frequency_indices)):
            t[ii,:,:,:], labels[ii,:,:] = self.get_matrix(ii)
        return t, labels


    def save_as_mat(self,fname_base):
        import scipy.io as io
        fb = str(fname_base)
        io.savemat(fb+"_iso.mat", {'isomat':self.iso_matrices,
            'freqs':self.frequencies})