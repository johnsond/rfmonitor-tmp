#!/usr/bin/env python

"""
Note:    
    Raw Measurement - sampled RF data across spectrum over both channels
    Iso Measurement - Spectrum measurement that has source separation algorithm applied 
"""

class Receiver():
    def __init__(self):
        pass

  
    def get_full_spectrum(self):
        """ Return power vs frequency
        """
        raise NotImplementedError("Subclass implements this method")

    def get_samples(self, frequency, num_samples):
        """ return `num_samples` complex samples from the receiver
        """
        raise NotImplementedError("Subclass implements this method")


    def reconfigure_receiver(self, gain, channels):
        """ set the gain value and the number of channels for the receiver. 
        """
        raise NotImplementedError("Subclass implements this method")

        
  
