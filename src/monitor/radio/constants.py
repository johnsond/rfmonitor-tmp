""" Relevant constants are stored here 
"""
import numpy as np

# ~6GHz to cover (5993 MHz). At 30.72 instantaneous MHz, this will need 195 iterations
# TODO move this out of radio

# the name of the packaged file that stores iso_matrices. Can be overridden
CAL_DATA_FNAME = "cal_data.pkl" 
CAL_DATA_REF = "cal_data_ref.pkl"

# define the frequency range for center frequencies
START_FREQ=100e6
END_FREQ=6e9 # note that the list is not upper-bound inclusive. I.e. cf in [start, end)

# maximum instantaneous bandwidth is equal to the maximum sampling rate
MAX_INST_BW = 30.72e6
ANALOG_BW_CUTOFF_FACTOR = 0.9
ANALOG_BW = MAX_INST_BW * ANALOG_BW_CUTOFF_FACTOR # 28e6

# a definitive list of the center frequencies that we will tune to
#CENTER_FREQS = np.arange(START_FREQ, END_FREQ, ANALOG_BW)
CENTER_FREQS = np.arange(START_FREQ, END_FREQ, ANALOG_BW*0.9)


# actual bounds of the spectrum collected
REAL_COVERAGE_LOWER=CENTER_FREQS[0]-MAX_INST_BW/2
REAL_COVERAGE_UPPER=CENTER_FREQS[-1]+MAX_INST_BW/2

# how many times will we need to change the frequency to cover whole spectrum
NUM_STEPS = len(CENTER_FREQS)

# specify the size of the FFT
# larger gives better frequency resolution, assume stationarity over N samples
FFT_SIZE = 2**9
N = int(np.floor(FFT_SIZE*ANALOG_BW_CUTOFF_FACTOR)) # reject frequencies above the analog bandwidth cutoff 
if N % 2 == 1:
    N+=1  # N should be even
N_REMOVE = (FFT_SIZE-N)//2  # how many bins to remove on both sides of the array

# remove slightly more
#N_REMOVE_EXTRA=20
#N_REMOVE += N_REMOVE_EXTRA
#N -= 2*N_REMOVE_EXTRA 

# use two channels
CHANNELS=(0,1) # RX1=0, RX2=1

# set the default gain value
GAIN=30 # not in dBm. Max gain is ~89

# NOTE: removed averaging
N_BINS_OUT_PER_STEP = N
RES_HZ = MAX_INST_BW/N_BINS_OUT_PER_STEP

# total data points collected
N_BINS_TOTAL = N_BINS_OUT_PER_STEP * NUM_STEPS
FREQ_VALS_ALL = np.arange(START_FREQ, END_FREQ, RES_HZ)

# uncertainty factor for measurements
UNCERT_FACTOR = 0.001

# Calibration constants
CAL_M=FFT_SIZE # output size of dft sampled
CAL_K=10 # number of averages 
CAL_N=CAL_M*CAL_K # collect this many samples 

# ISO avg
ISO_K=2**10

# Logging utilities
WARNC = '\033[93m'
NORMC = '\033[0m'
