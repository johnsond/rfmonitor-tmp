import numpy as np
from . import receiver 
from . import constants as C
import time

class MockReceiver(receiver.Receiver):
    def __init__(self, conf):
        print("Using mock receiver")
    
    def get_full_spectrum(self):
        """ 
        """
        devs, tstamps, freqs, powers = np.array([]),np.array([]),np.array([]),np.array([])
        for ii in range(C.NUM_STEPS):
            devs = np.append(devs, "mockdev")
            tstamps = np.append(tstamps, time.time())
            freqs = np.append(freqs, (C.START_FREQ + ii*C.MAX_INST_BW)/1e6)
            powers = np.append(powers, np.random.uniform(-70, -50))
            
        return devs,tstamps, freqs, powers
    
    def get_samples(self, frequency, num_samples):
        """ 
        """
        return np.random.random((2,num_samples))


    def reconfigure_receiver(self, gain, channels):
        """ 
        """
        pass
