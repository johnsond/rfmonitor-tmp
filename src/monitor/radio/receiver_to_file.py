import receiver
import numpy as np
import time
import scipy.io

# use fromfile() to recover
# writes bytes in C order

f_format = "%Y%m%d-%H%M%S"

rx = receiver.Receiver()
bins, labels, rx1, rx2 = rx.get_spectrum(debug=True, full_resolution=True)

print(bins.shape)
print(labels.shape)
print(rx1.shape)
print(rx2.shape)


time_str = time.strftime(f_format)
fbins = open('bins_' + time_str + '.dat', 'w+b')
bins.tofile(fbins) 
fbins.close()


flabels = open('labels_' + time_str + '.dat', 'w+b')
labels.tofile(flabels) 
flabels.close()

f1 = open('rx1_' + time_str + '.dat', 'w+b')
rx1.tofile(f1) 
f1.close()

f2 = open('rx2_' + time_str + '.dat', 'w+b')
rx2.tofile(f2) 
f2.close()

scipy.io.savemat('bins_' + time_str + '.mat', mdict={'arr':bins})
scipy.io.savemat('labels_' + time_str + '.mat', mdict={'arr':labels})
scipy.io.savemat('rx1_' + time_str + '.mat', mdict={'arr':rx1})
scipy.io.savemat('rx2_' + time_str + '.mat', mdict={'arr':rx2})
