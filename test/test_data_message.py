import unittest
import monitor.net.data_message as message
import numpy as np
import time

TS = int(time.time())
MESSAGE=""
EXAMPLE_ROW1=("dev1", TS, 2400e6, -70.0)

class TestDevice(unittest.TestCase):

    def create_new_msg(self, size):
        return message.DataMessage(size)

    def test_count_at_zero(self):
        msg = self.create_new_msg(10)
        self.assertTrue(msg.get_count() == 0)

    def test_add_to_empty(self):
        msg = self.create_new_msg(10)
        msg.add_tuple(EXAMPLE_ROW1[0], EXAMPLE_ROW1[1], EXAMPLE_ROW1[2], EXAMPLE_ROW1[3]) 
        self.assertTrue(msg.get_count() == 1)



        