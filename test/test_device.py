import unittest
import monitor.radio.device as device
import monitor.radio.signal_utils as su
import numpy as np

TEST_CF = 2400e6

class TestDevice(unittest.TestCase):

    def setUp(self):
        self.device  = device.Device()
    
    def test_get_temp(self):
        temp = self.device.get_temperature()
        self.assertTrue(temp > 0 and temp != 0)

    def test_get_RSSI(self):
        rssi = self.device.get_rssi()
        self.assertTrue(rssi < 0 and rssi != 0)

    def test_get_serial_number(self):
        sn = self.device.get_serial_number()
        self.assertTrue(sn != "")

    def test_get_LO_locked(self):
        locked = self.device.get_LO_locked()
        self.assertIs(type(locked), bool)


    def test_get_N_samples(self):
        N=2**20
        samps = self.device.get_N_samples(TEST_CF,N)
        self.assertTrue(np.size(samps) == N*2)
        self.assertTrue(su.energy(samps[0,:]) > 0)  
        self.assertTrue(su.energy(samps[1,:]) > 0)
        return

    def test_get_dft(self):
        N=2**20
        samps = self.device.get_dft(TEST_CF,N)
        self.assertTrue(np.size(samps) == N*2)
        self.assertTrue(su.energy(samps[0,:]) > 0)  
        self.assertTrue(su.energy(samps[1,:]) > 0)
        return

    def test_get_psd(self):
        N=2**20
        samps,labels = self.device.get_spectrum(TEST_CF,N)
        self.assertTrue(np.size(samps) == N*2)
        self.assertTrue(np.size(labels) == N*2)
        self.assertTrue(su.energy(samps[0,:]) > 0)  
        self.assertTrue(su.energy(samps[1,:]) > 0)
        return

    def test_get_dft_avg(self):
    
        # design for M
        M=512
        K = 100
        N=M*K
        
        samps = self.device.get_dft_avg(TEST_CF,N, K)
        self.assertTrue(np.size(samps) == M*2)
        self.assertTrue(su.energy(samps[0,:]) > 0)  
        self.assertTrue(su.energy(samps[1,:]) > 0)
        return

if __name__ == '__main__':
    unittest.main()

    
