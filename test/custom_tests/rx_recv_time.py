#!/usr/bin/env python 

import uhd
import uhd.libpyuhd as lib
import numpy as np
import time

usrp = uhd.usrp.MultiUSRP()

chan = 0
freq = 2.4e6
rate = 12e6 # fast enough to make buffer/transfer the bottleneck
# 2040 samples is the max for one transport
print 2040/rate # gives the number of seconds to collect this much data
gain = 10


usrp.set_rx_rate(rate, chan)
usrp.set_rx_freq(lib.types.tune_request(freq), chan) 
usrp.set_rx_gain(gain, chan)

st_args = lib.usrp.stream_args("fc32", "sc16")
st_args.channels = (0,)
metadata = lib.types.rx_metadata()

streamer = usrp.get_rx_stream(st_args)
buffer_samps = streamer.get_max_num_samps()
print("Max # samps: " + str(buffer_samps))
recv_buffer = np.zeros((1, buffer_samps), dtype=np.complex64)

recv_samps = 0
stream_cmd = lib.types.stream_cmd(lib.types.stream_mode.start_cont)
stream_cmd.stream_now = True
streamer.issue_stream_cmd(stream_cmd) # -- does streaming start now? How long does this block for?

experiments = 100
times = []
for i in range(experiments):
    t1 = time.time()
    num_samps_recv = streamer.recv(recv_buffer, metadata)
    t2 = time.time()
    times.append(t2-t1)
                 
print("average time: " + str( np.sum(times) / len(times)))
                 
stream_cmd_stop = lib.types.stream_cmd(lib.types.stream_mode.stop_cont)
streamer.issue_stream_cmd(stream_cmd_stop) # do I need to stop before re-tuning? Do I need to empty the buffer? 
