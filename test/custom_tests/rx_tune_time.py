#!/usr/bin/env python 

import uhd
import uhd.libpyuhd as lib
import numpy as np
import time
import pprint
import matplotlib.pyplot as plt


usrp = uhd.usrp.MultiUSRP()

chan = 0
freq = 2.4e6
rate = 12e6
gain = 10

times = []
experiments = 5
#freqs =  [100e6, 150e6, 250e6, 350e6, 500e6, 800e6, 1100e6, 1800e6, 2700e6, 4000e6, 5500e6] # 70 MHz - 6 GHz
freqs = []
current_cf = np.floor(70e6 + 30.72e6/2)
for i in range(195):
    freqs.append(current_cf)
    current_cf = np.floor(current_cf  + 30.72e6)


print("max cf: " + str(max(freqs)))

for i in range(experiments):
    for f in freqs:
        t1 = time.time()
        usrp.set_rx_freq(lib.types.tune_request(f), chan) 
        t2 = time.time()
        times.append(t2-t1)

#pprint.pprint(times)
num_experiments = len(times)
avg_time = np.sum(times) / num_experiments
print(num_experiments)
print(max(times))
print(min(times))
print(times)
print("Average time to tune 1 channel", avg_time)

plt.hist(times, 20)
#fig = plt.figure()
#fig.savefig('hist.png')
plt.show()
