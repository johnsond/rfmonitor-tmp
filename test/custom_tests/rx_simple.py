#!/usr/bin/env python 

import uhd
import uhd.libpyuhd as lib
import numpy as np
import time

usrp = uhd.usrp.MultiUSRP()

chan = 0
freq = 2.4e6
rate = 12e6
gain = 10


usrp.set_rx_rate(rate, chan) # how long do each of these block for?
usrp.set_rx_freq(lib.types.tune_request(freq), chan) 
usrp.set_rx_gain(gain, chan)

st_args = lib.usrp.stream_args("fc32", "sc16")
st_args.channels = (0,)
metadata = lib.types.rx_metadata()

streamer = usrp.get_rx_stream(st_args)
buffer_samps = streamer.get_max_num_samps()
print("Max # samps: " + str(buffer_samps))
recv_buffer = np.zeros((1, buffer_samps), dtype=np.complex64)

recv_samps = 0
stream_cmd = lib.types.stream_cmd(lib.types.stream_mode.start_cont)
stream_cmd.stream_now = True
streamer.issue_stream_cmd(stream_cmd) # -- does streaming start now? How long does this block for?


num_samps_recv = streamer.recv(recv_buffer, metadata)
print("received: " + str(num_samps_recv) + " samples")

stream_cmd_stop = lib.types.stream_cmd(lib.types.stream_mode.stop_cont)
streamer.issue_stream_cmd(stream_cmd_stop)

