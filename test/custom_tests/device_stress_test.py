#!/usr/bin/env python

import sys
sys.path.insert(0, "../")

import radio.device
import matplotlib.pyplot as plt
import numpy as np
import time
import radio.signal_utils as su

device = radio.device.Device()
num_samps_req = device.get_num_samps_requested()
rate = device.get_sample_rate()

funcs = [device.get_samples_cont, device.get_samples_stepped] #, device.get_samples_cont_exp, device.get_samples_ettus]

""" Test 1 """
""" Test notes: 
> Every four frequency changes, we typically get the ODD sequence printed (regardless of freq step size, or freq value)
  The D refers to a discontinuity in the sequence numbers
  The O refers to an overflow 
"""
for func in funcs:

    #freqs = [700e6, 5200e6]
    freqs = [700e6, 1700e6, 2400e6, 3500e6, 5200e6]
    iterations = 50
    time_for_freq_change = 0.15 # 150 ms for large steps based on empirical data
    ideal_time = iterations*(num_samps_req/rate + time_for_freq_change)
    num_total_errors = 0
    num_total_samps = 0
    tstart = time.time()
    for i in range(iterations):
        #print("{}%, {}MHz".format(float(i)/iterations*100, freqs[i % len(freqs)]/1e6))
        samps, num_errors = func(freqs[i % len(freqs)])
        num_total_samps += len(samps[0,:])
        num_total_errors += num_errors 


    tend = time.time()
    print("\n-------------------------------------------------------------")
    print("test1 - function: {}".format(func))
    print("test1 - continuous, large f steps")
    print("test1 - num errors: " + str(num_total_errors))
    print("test1 - num errors per iteration: " + str(num_total_errors/float(iterations)))
    print("test1 - num samples expected: " + str(num_samps_req * iterations))
    print("test1 - num_samples received " +str(num_total_samps))
    print("test1 - expected run time: " +str(ideal_time))
    print("test1 - actual run time: " + str(tend-tstart))



    """ Test 2 """
    freqs = [2400e6, 2401e6]
    iterations = 100000 # we need a significant number to test firmware issues
    time_for_freq_change = 0.004 # 4 ms for small steps 
    ideal_time = iterations*(num_samps_req/rate + time_for_freq_change)
    num_total_errors = 0
    num_total_samps = 0
    tstart = time.time()
    for i in range(iterations):
        if i % 200 == 0:
            print("{}%, {}MHz".format(float(i)/iterations*100, freqs[i % len(freqs)]/1e6))      
        samps, num_errors = func(freqs[i % len(freqs)])
        num_total_samps += len(samps[0,:])
        num_total_errors += num_errors
            

    tend = time.time()
    print("\n-------------------------------------------------------------")
    print("test2 - continuous, small f, {} iterations".format(iterations))
    print("test2 - num errors: " + str(num_total_errors))
    print("test2 - num errors per iteration: " + str(num_total_errors/float(iterations)))
    print("test2 - num samples expected: " + str(num_samps_req * iterations))
    print("test2 - num_samples received " +str(num_total_samps))
    print("test2 - expected run time: " +str(ideal_time))
    print("test2 - actual run time: " + str(tend-tstart))
    
sys.exit(0)

freqs = [1700e6, 2400e6, 3500e6, 5200e6]
iterations = 50
time_for_freq_change = 0.15 # 150 ms for large steps
ideal_time = iterations*(num_samps_req/rate + time_for_freq_change)
num_total_errors = 0
num_total_samps = 0
tstart = time.time()
for i in range(iterations):
    samps, num_errors = device.get_samples(freqs[i % len(freqs)])
    num_total_samps += len(samps[0,:])
    num_total_errors += num_errors

tend = time.time()
print("\n-------------------------------------------------------------")
print("test3 - stepped, large f steps")
print("test3 - num errors: " + str(num_total_errors))
print("test3 - num errors per iteration: " + str(num_total_errors/float(iterations)))
print("test3 - num samples expected: " + str(num_samps_req * iterations))
print("test3 - num_samples received " +str(num_total_samps))
print("test3 - expected run time: " +str(ideal_time))
print("test3 - actual run time: " + str(tend-tstart))



freqs = [2400e6]
result,num_errors = device.get_samples(freqs[0])
print("collecting at rate: {:.2f} MHz".format(device.usrp.get_rx_rate()/1e6))
print("rx BW: {:.1f} MHz".format(device.usrp.get_rx_bandwidth()/1e6))
print("rx freq: {:.1f} MHz".format(device.usrp.get_rx_freq()/1e6))
width = int(num_samps_req)
bins_chan0 = su.psd(width, result[0,0:width])
bins_chan1 = su.psd(width, result[1,0:width])
binwidth = rate/width
half_intv_len = width / 2
xwindx = np.array(range(-half_intv_len,half_intv_len))*binwidth
plt.subplot(211)
plt.plot(xwindx, bins_chan1, xwindx, bins_chan0)
plt.subplot(212)
binn = 128
newbins0, newlabels0 = su.bin_avg(binn, bins_chan0, xwindx)
newbins1, newlabels1 = su.bin_avg(binn, bins_chan1, xwindx)
plt.plot(newlabels1, newbins1,newlabels0, newbins0)
plt.show()

