This application currently only has a handful of dependencies
* Python, version=[3.6, )
* UHD, version=[3.14,), driver for the Ettus software radio
* NumPy, version=[1.13,), Python high-performance numerical computation library
For calibration only:
* SciPy, version=[], a scientific computing library (signal package used)
* NTPlib, version=[], a library for using Network Time Protocol
For graphical utilities only:
* Matplotlib, version=[], used mostly for visualizing signals

<br>
This install note assumes an x86 Ubuntu 18 (bionic) OS  
<br>

To install dependendencies, run `scripts/install_packages.sh`
To install the monitor into standard locations run `scripts/install_monitor.sh`
